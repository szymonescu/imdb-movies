from lib.fetchers.movie_fetcher import MovieFetcher
from lib.fetchers.requests_fetcher import RequestsFetcher
from lib.info.handlers import MovieInfoHandler
from lib.savers.csv_saver import CSVSaver
from lib.savers.data_saver import DataSaver
from lib.sources.imdb.decoders.extractor import Extractor
from lib.sources.imdb.decoders.html_finder import HTMLFinder
from lib.sources.imdb.decoders.html_reader import HTMLReader
from lib.sources.imdb.imdb_data_provider import IMDBDataProvider
from lib.sources.omdb.JSONDeserializer import JSONDeserializer
from lib.sources.omdb.omdb_data_provider import OMDBDataProvider


html_finder = HTMLFinder()
extractor = Extractor(finder=html_finder)
requests_fetcher = RequestsFetcher()
movie_fetcher = MovieFetcher(fetcher_lib=requests_fetcher)
html_parser = HTMLReader()
json_deserializer = JSONDeserializer()

imdb_data_provider = IMDBDataProvider(
    fetcher=movie_fetcher, html_parser=html_parser, extractor=extractor)
omdb_data_provider = OMDBDataProvider(
    fetcher=movie_fetcher, deserializer=json_deserializer)
csv_saver = CSVSaver()
data_saver = DataSaver(saver=csv_saver)
movie_info_handler = MovieInfoHandler(
    imdb_data_provider=imdb_data_provider,
    omdb_data_provider=omdb_data_provider,
    data_saver=data_saver
)
