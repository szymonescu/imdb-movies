class MovieFetcher:
    def __init__(self, fetcher_lib):
        self.fetcher_lib = fetcher_lib

    def fetch(self, url):
        return self.fetcher_lib.fetch(url)
