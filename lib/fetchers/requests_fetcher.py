import requests


class RequestsFetcher:
    def __init__(self):
        self.fetcher = requests

    def fetch(self, url):
        return self.fetcher.get(url)
