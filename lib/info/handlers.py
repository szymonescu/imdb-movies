from lib.info.exceptions import TooManyMoviesToFetch


class MovieInfoHandler:
    def __init__(self, imdb_data_provider, omdb_data_provider, data_saver):
        self.imdb_data_provider = imdb_data_provider
        self.omdb_data_provider = omdb_data_provider
        self.data_saver = data_saver

    def handle(self, url, from_=0, to=75):
        if abs(from_ - to) > 75:
            raise TooManyMoviesToFetch(
                'Can only get data for up to 75 movies at once')
        data = self.imdb_data_provider.provide(url)
        data = self.omdb_data_provider.provide(data[from_:to])
        return self.data_saver.save(data)
