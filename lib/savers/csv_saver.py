import csv


class CSVSaver:
    def save(self, data, filename):
        with open(filename, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=';')
            for row in data:
                writer.writerow([row['Title'], row['Year']])
        return f'Data written to \'{filename}\''
