class DataSaver:
    def __init__(self, saver):
        self.saver = saver

    def save(self, data, filename='movies.csv'):
        return self.saver.save(data, filename)
