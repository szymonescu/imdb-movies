class Extractor:
    def __init__(self, finder):
        self.finder = finder

    def extract(self, html):
        data = self.finder.find_all(html)
        return self.finder.find(data)
