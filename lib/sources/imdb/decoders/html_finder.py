class HTMLFinder:
    def find_all(self, html):
        return html.findAll("td", {"class": "titleColumn"})

    def find(self, html):
        result = []
        for row in html:
            href = row.find("a", href=True)['href']
            result.append(href.split('/')[2])
        return result
