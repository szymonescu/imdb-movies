from bs4 import BeautifulSoup


class HTMLReader:
    def read(self, html):
        return BeautifulSoup(html, "html.parser")
