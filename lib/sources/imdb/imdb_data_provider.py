class IMDBDataProvider:
    def __init__(self, fetcher, html_parser, extractor):
        self.fetcher = fetcher
        self.html_parser = html_parser
        self.extractor = extractor

    def provide(self, url):
        data = self.fetcher.fetch(url)
        html = self.html_parser.read(data.content)
        return self.extractor.extract(html)
