from operator import itemgetter


class OMDBDataProvider:
    def __init__(self, fetcher, deserializer):
        self.fetcher = fetcher
        self.deserializer = deserializer
        self.url = 'http://www.omdbapi.com/?i={}'

    def provide(self, movie_ids, sort_by='Year'):
        result = []
        for movie_id in movie_ids:
            response = self.fetcher.fetch(self.url.format(movie_id))
            result.append(self.deserializer.deserialize(response))
        return sorted(result, key=itemgetter(sort_by))
